# Entity Fields Search

​Module to Entity Fields Search by Title.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/entitytype_filter).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/entitytype_filter).

## Table of contents

- Installation
- How To Use
- Maintainers

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## How To Use
#### [ Admin Toolbar Shortcuts: Content->Entity Fields Search->  <select>
  <option >By Entity name</option>
  <option >By Field Type</option>
  </select> ]

## 1. Search Fields by Entity Name
Access the form at (/admin/entitytypes-filter).

### Fields

- #### "Select Entity Type" Field

     Select the Entity Type Block Types, Content Types or Paragraph Types whose Entity is to be Searched.

- #### "Search Entity Type By Title" Field

     Dynamic Field Dependent Upon
  <select>
  <option value="" selected="selected">- Select a value -</option>
  <option value="block_content" >Block Types</option>
  <option value="node">Content Type</option>
  <option value="paragraph">Paragraph Types</option>
  </select> (Select Entity Type) Field. Enter The Name of Entity and Click On Search Button, It Will List All The Fields.

## 2. Search Fields by Field Type
Access the form at (/admin/fieldtypes-filter).

### Fields

- #### "Select Entity Type" Field

- #### "Select Field Type" Field

## Maintainers

Current maintainers:

- [Keshav Patel (keshav-patel)](https://www.drupal.org/u/keshav-patel)

- [Pradhuman Jain (pradhumanjainOSL)](https://www.drupal.org/u/pradhumanjainOSL)

- [Ashish Dwivedi (adwivedi008)](https://www.drupal.org/u/adwivedi008)
​
