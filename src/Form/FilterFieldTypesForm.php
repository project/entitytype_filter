<?php

namespace Drupal\entitytype_filter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class of FilterFieldTypesForm.
 */
class FilterFieldTypesForm extends FormBase {

  /**
   * Account Interface.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Entity Type Manager Interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * Entity Field Manager Interface.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The CacheBackendInterface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Constructs a new FilterFieldTypesForm object.
   */
  public function __construct(AccountInterface $account, EntityTypeManagerInterface $entity_type_manager, FieldTypePluginManagerInterface $field_type_plugin_manager, EntityFieldManagerInterface $entity_field_manager, MessengerInterface $messenger, CacheBackendInterface $staticCache) {
    $this->account = $account;
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldTypePluginManager = $field_type_plugin_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->messenger = $messenger;
    $this->cache = $staticCache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('entity_field.manager'),
      $container->get('messenger'),
      $container->get('cache.default'),
    );
  }

  /**
   * Form ID of FilterFieldTypesForm.
   */
  public function getFormId() {
    return 'entitytype_filter_form';
  }

  /**
   * Builds FilterFieldTypesForm.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $output = [];

    if (isset($form_state->getValues()['conditional-wrapper']['fselect_entity_type']) && isset($form_state->getValues()['conditional-wrapper']['fselect_field_type'])) {
      $selected_entity_type = $form_state->getValues()['conditional-wrapper']['fselect_entity_type'];
      if ($selected_entity_type == 'none') {
        $this->messenger->addWarning("Please select the Entity Type.");
      }
      else {
        $cache_id = 'entity_storage_' . $selected_entity_type;

        $selected_field_type = $form_state->getValues()['conditional-wrapper']['fselect_field_type'];

        $entity_type_ids = [
          'block_content_type' => 'block_content',
          'paragraphs_type' => 'paragraph',
          'node_type' => 'node',
        ];

        $fields_data = [];

        $cache = $this->cache->get($cache_id);
        if ($cache) {
          $entity_types = $cache->data;
        }
        else {
          $entity_types = $this->entityTypeManager->getStorage($selected_entity_type)->loadMultiple();
          $this->cache->set($cache_id, $entity_types, 86400);
        }

        foreach ($entity_types as $key => $type) {
          $type_id = $type->id();
          $fields = $this->getEntityFieldsData($entity_type_ids[$selected_entity_type], $type_id);
          $fields_data += $fields;
        }
        $selected_field_type = isset((explode(':', $selected_field_type))[2]) ? (explode(':', $selected_field_type))[2] : $selected_field_type;
        foreach ($fields_data as $field) {
          $field_type = isset($field->getSettings()['handler']) ? explode(':', $field->getSettings()['handler'])[1] : $field->getType();

          if ($selected_field_type == 'all') {
            $output[$field->id()] = [
              'bundle_name' => $field->getTargetBundle(),
              'field_label' => $field->label(),
              'field_name' => $field->getName(),
              'field_type' => $field_type,
            ];
          }
          elseif ($selected_field_type == $field_type) {
            $output[$field->id()] = [
              'bundle_name' => $field->getTargetBundle(),
              'field_label' => $field->label(),
              'field_name' => $field->getName(),
              'field_type' => $field_type,
            ];
          }
        }
      }
    }

    $form['#tree'] = TRUE;
    $header = [
      'bundle_name' => $this->t('<b>Bundle Machine Name</b>'),
      'field_label' => $this->t('<b>Field Label</b>'),
      'field_name' => $this->t('<b>Field Machine Name</b>'),
      'field_type' => $this->t('<b>Field Type</b>'),
    ];

    $form['filter_entity_types_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $output,
      '#weight' => 35,
      '#empty' => $this->t('No Fields Exists.'),
    ];

    $entity_type_options = [
      'none' => '- Select a Value -',
      'block_content_type' => 'Block Types',
      'node_type' => 'Content Type',
      'paragraphs_type' => 'Paragraph Types',
    ];

    $form['conditional-wrapper'] = [
      '#prefix' => '<div class="conditional-wrapper">',
      '#suffix' => '</div>',
    ];
    $form['conditional-wrapper']['fselect_entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Entity Type'),
      '#options' => $entity_type_options,
      '#default_value' => $entity_type_options['none'],
    ];

    $field_type_options = [];
    $field_type_options['all'] = 'All';
    foreach ($this->fieldTypePluginManager->getGroupedDefinitions($this->fieldTypePluginManager->getUiDefinitions()) as $category => $field_types) {
      foreach ($field_types as $name => $field_type) {
        $field_type_options[$name] = ($field_type['label'])->__toString();
      }
    }

    $form['conditional-wrapper']['fselect_field_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Field Type'),
      '#options' => $field_type_options,
      '#default_value' => $field_type_options['all'],
    ];

    $form['download_table_csv'] = [
      '#type' => 'button',
      '#value' => '',
      '#weight' => 40,
      '#attributes' => [
        'id' => 'download_fields_table_as_csv',
        'title' => 'Download as CSV',
      ],
    ];

    $form['conditional-wrapper']['actions'] = [
      '#type' => 'actions',
      '#weight' => 30,
    ];
    $form['conditional-wrapper']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#attributes' => [
        'class' => ['button--primary'],
      ],
    ];

    $form['#attached']['library'][] = 'entitytype_filter/filter_fields_by_type';
    return $form;
  }

  /**
   * Implements validateForm().
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Implements submitForm().
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * Implments recursiveSearchKeyMap().
   */
  public function recursiveSearchKeyMap($needle, $haystack) {
    foreach ($haystack as $first_level_key => $value) {
      if ($needle === $first_level_key) {
        return [$first_level_key];
      }
      elseif (is_array($value)) {
        $callback = $this->recursiveSearchKeyMap($needle, $value);
        if ($callback) {
          return ($value[$needle]->__toString());
        }
      }
    }
    return FALSE;
  }

  /**
   * Implements getEntityFieldsData().
   */
  public function getEntityFieldsData($entity_type_id, $bunde_id) {
    $fields = [];
    $field_defs = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bunde_id);
    $base_field_defs = $this->entityFieldManager->getBaseFieldDefinitions($entity_type_id, $bunde_id);

    foreach ($field_defs as $key => $value) {
      if (!array_key_exists($key, $base_field_defs)) {
        $fields[$entity_type_id . '.' . $bunde_id . '.' . $key] = $value;
      }
    }
    return $fields;
  }

}
