<?php

namespace Drupal\entitytype_filter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\field\FieldConfigInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;

/**
 * Class of FilterEntityTypesForm.
 */
class FilterEntityTypesForm extends FormBase {

  /**
   * Account Interface.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Entity Type Manager Interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypePluginManager;

  /**
   * Constructs a new FilterEntityTypesForm object.
   */
  public function __construct(AccountInterface $account, EntityTypeManagerInterface $entity_type_manager, FieldTypePluginManagerInterface $field_type_plugin_manager) {
    $this->account = $account;
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldTypePluginManager = $field_type_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.field.field_type'),
    );
  }

  /**
   * Form ID of FilterEntityTypesForm.
   */
  public function getFormId() {
    return 'entitytype_filter_form';
  }

  /**
   * Builds FilterEntityTypesForm.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $output = [];

    if ($form_state->getValue('autocomplete_ajax_container')['filter_entity_types']) {
      $entity_type = $form_state->getValue('select_entity_type');
      $entity_name = EntityAutocomplete::extractEntityIdFromAutocompleteInput($form_state->getValue('autocomplete_ajax_container')['filter_entity_types']);
      $fields = array_filter(
        \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, $entity_name),
        function ($fieldDefinition) {
          return $fieldDefinition instanceof FieldConfigInterface;
        }
      );
      foreach ($fields as $field) {
        // Gather valid field types.
        $field_type_options = [];
        foreach ($this->fieldTypePluginManager->getGroupedDefinitions($this->fieldTypePluginManager->getUiDefinitions()) as $category => $field_types) {
          foreach ($field_types as $name => $field_type) {
            $field_type_options[$category][$name] = $field_type['label'];
          }
        }
        $field_machine_name = $this->recursiveSearchKeyMap($field->getType(), $field_type_options) ? $this->recursiveSearchKeyMap($field->getType(), $field_type_options) : $field->getType();

        if (($field->getType() == 'entity_reference') || ($field->getType() == 'entity_reference_revisions')) {
          $field_type_name = $field->getSettings()['target_type'];
          if ($field->getType() == 'entity_reference') {
            $ref = $this->recursiveSearchKeyMap('field_ui:entity_reference:' . $field_type_name, $field_type_options);
            $entity_ref_field_name = $ref ? $ref : $field_type_name;

            $output[$field->getName()] = [
              'field_label' => $this->getEntityBasedFieldUrl($entity_type, $entity_name, $field),
              'field_type' => $entity_ref_field_name,
            ];
          }
          elseif ($field->getType() == 'entity_reference_revisions') {
            $para_type = isset($field->getSettings()['handler_settings']['target_bundles']) ? array_keys($field->getSettings()['handler_settings']['target_bundles'])[0] : 'No Paragraph Type Referenced For This Field';

            $ref_rev = $this->recursiveSearchKeyMap('field_ui:entity_reference_revisions:' . $field_type_name, $field_type_options);
            $entity_ref_rev_field_name = $ref_rev ? $ref_rev : $field_type_name;
            if ($para_type == 'No Paragraph Type Referenced For This Field') {
              $output[$field->getName()] = [
                'field_label' => $this->getEntityBasedFieldUrl($entity_type, $entity_name, $field),
                'field_type' => $this->t($entity_ref_rev_field_name . ' [ ' . $this->getEntityBasedFieldUrl($entity_type, $entity_name, $field, $para_type) . ' ]'),
              ];
            }
            else {
              $output[$field->getName()] = [
                'field_label' => $this->getEntityBasedFieldUrl($entity_type, $entity_name, $field),
                'field_type' => $this->t($entity_ref_rev_field_name . ' <b>[Paragraph Type : <a href="/admin/structure/paragraphs_type/' . $para_type . '/fields" target="_blank" >' . $para_type . '</a>]</b>'),
              ];
            }
          }
        }
        else {
          $output[$field->getName()] = [
            'field_label' => $this->getEntityBasedFieldUrl($entity_type, $entity_name, $field),
            'field_type' => $field_machine_name,
          ];
        }
      }
    }

    $form['#tree'] = TRUE;
    $header = [
      'field_label' => $this->t('<b>Field Label</b>'),
      'field_type' => $this->t('<b>Field Type</b>'),
    ];

    $form['filter_entity_types_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $output,
      '#weight' => '50',
      '#empty' => $this->t('No Fields Exists.'),
    ];

    // AJAX DEPENDENT FIELDS
    // Get the form values and raw input (unvalidated values).
    $values = $form_state->getValues();

    // Define a wrapper id to populate new content into.
    $ajax_wrapper = 'entity-ajax-wrapper';

    // Sector.
    $form['select_entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Entity Type'),
      '#empty_value' => '',
      '#empty_option' => '- Select a value -',
      '#default_value' => ($values['select_entity_type'] ?? ''),
      '#options' => [
        'block_content' => 'Block Types',
        'node' => 'Content Type',
        'paragraph' => 'Paragraph Types',
      ],
      '#weight' => '-50',
      '#ajax' => [
        'callback' => [$this, 'selectEntityTypeChange'],
        'event' => 'change',
        'wrapper' => $ajax_wrapper,
      ],
      '#prefix' => '<div class="custom-wrap">',
      '#suffix' => '</div>',
    ];

    // Build a wrapper for the ajax response.
    $form['autocomplete_ajax_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $ajax_wrapper,
      ],
    ];

    // ONLY LOADED IN AJAX RESPONSE OR IF FORM STATE VALUES POPULATED.
    if (!empty($values) && !empty($values['select_entity_type'])) {
      $entity_type_names = [
        'block_content' => 'Block Types',
        'node' => 'Content Types',
        'paragraph' => 'Paragraph Types',
      ];
      if (array_key_exists($values['select_entity_type'], $entity_type_names)) {
        $selected_entity_type = $entity_type_names[$values['select_entity_type']];
      }
      $form['autocomplete_ajax_container']['filter_entity_types'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Search ' . $selected_entity_type . ' By Title'),
        '#weight' => '10',
        '#maxlength' => 1280,
        '#description' => '',
        '#autocomplete_route_name' => 'entitytype_filter.autocomplete.' . $values['select_entity_type'] . '_entity_type',
      ];
    }
    // END AJAX DEPENDENT FIELDS.
    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => '10',
      '#prefix' => '<div class="custom-wrap-1">',
      '#suffix' => '</div>',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#attributes' => [
        'class' => ['button--primary'],
      ],
    ];

    $form['#attached']['library'][] = 'entitytype_filter/filter_field';
    return $form;
  }

  /**
   * Implements validateForm().
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Implements submitForm().
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * Implements recursiveSearchKeyMap().
   */
  public function recursiveSearchKeyMap($needle, $haystack) {
    foreach ($haystack as $first_level_key => $value) {
      if ($needle === $first_level_key) {
        return [$first_level_key];
      }
      elseif (is_array($value)) {
        $callback = $this->recursiveSearchKeyMap($needle, $value);
        if ($callback) {
          return ($value[$needle]->__toString());
        }
      }
    }
    return FALSE;
  }

  /**
   * Implements selectEntityTypeChange().
   */
  public function selectEntityTypeChange(array $form, FormStateInterface $form_state) {
    // Return the element that will replace the wrapper (we return itself).
    return $form['autocomplete_ajax_container'];
  }

  /**
   * Implements getEntityBasedFieldUrl().
   */
  public function getEntityBasedFieldUrl($entity_type, $entity_name, $field, $title = NULL) {

    $label = $title ?? $field->label();
    if ($entity_type == 'block_content') {
      return $this->t('<a href="/admin/structure/block/block-content/manage/' . $entity_name . '/fields/' . $entity_type . '.' . $entity_name . '.' . $field->getName() . '" target="_blank" >' . $label . '</a>');
    }
    elseif ($entity_type == 'node') {
      return $this->t('<a href="/admin/structure/types/manage/' . $entity_name . '/fields/' . $entity_type . '.' . $entity_name . '.' . $field->getName() . '" target="_blank" >' . $label . '</a>');
    }
    elseif ($entity_type == 'paragraph') {
      return $this->t('<a href="/admin/structure/paragraphs_type/' . $entity_name . '/fields/' . $entity_type . '.' . $entity_name . '.' . $field->getName() . '" target="_blank" >' . $label . '</a>');
    }
  }

}
