<?php

namespace Drupal\entitytype_filter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\Element\EntityAutocomplete;

/**
 * Defines a route controller for watches autocomplete form elements.
 */
class EntitiesAutoCompleteController extends ControllerBase {

  /**
   * Entity Type Manager Interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Handler for autocomplete request for block types.
   */
  public function handleBlockAutocomplete(Request $request) {
    $results = [];
    // Get the typed string from the URL, if it exists.
    $input = $request->query->get('q');
    if (!$input) {
      return new JsonResponse($results);
    }
    $input = Xss::filter($input);
    $query = $this->entityTypeManager->getStorage('block_content_type')->getQuery()
      ->condition('label', $input, 'CONTAINS')
      ->sort('created', 'DESC')
      ->range(0, 10);

    $ids = $query->execute();
    $ids = array_keys($ids);

    $nodes = $ids ? $this->entityTypeManager->getStorage('block_content_type')->loadMultiple($ids) : [];
    foreach ($nodes as $node) {
      $label = [
        $node->label(),
        '<small>(' . $node->id() . ')</small>',
      ];
      $results[] = [
        'value' => EntityAutocomplete::getEntityLabels([$node]),
        'label' => implode(' ', $label),
      ];
    }
    return new JsonResponse($results);
  }

  /**
   * Handler for autocomplete request for content types.
   */
  public function handleNodeAutocomplete(Request $request) {
    $results = [];
    // Get the typed string from the URL, if it exists.
    $input = $request->query->get('q');
    if (!$input) {
      return new JsonResponse($results);
    }
    $input = Xss::filter($input);
    $query = $this->entityTypeManager->getStorage('node_type')->getQuery()
      ->condition('name', $input, 'CONTAINS')
      ->sort('created', 'DESC')
      ->range(0, 10);
    $ids = $query->execute();
    $ids = array_keys($ids);

    $nodes = $ids ? $this->entityTypeManager->getStorage('node_type')->loadMultiple($ids) : [];
    foreach ($nodes as $node) {
      $label = [
        $node->label(),
        '<small>(' . $node->id() . ')</small>',
      ];
      $results[] = [
        'value' => EntityAutocomplete::getEntityLabels([$node]),
        'label' => implode(' ', $label),
      ];
    }
    return new JsonResponse($results);
  }

  /**
   * Handler for autocomplete request for paragraph types.
   */
  public function handleParagraphAutocomplete(Request $request) {
    $results = [];
    // Get the typed string from the URL, if it exists.
    $input = $request->query->get('q');
    if (!$input) {
      return new JsonResponse($results);
    }
    $input = Xss::filter($input);
    $query = $this->entityTypeManager->getStorage('paragraphs_type')->getQuery()
      ->condition('label', $input, 'CONTAINS')
      ->sort('created', 'DESC')
      ->range(0, 10);

    $ids = $query->execute();
    $ids = array_keys($ids);

    $nodes = $ids ? $this->entityTypeManager->getStorage('paragraphs_type')->loadMultiple($ids) : [];
    foreach ($nodes as $node) {
      $label = [
        $node->label(),
        '<small>(' . $node->id() . ')</small>',
      ];
      $results[] = [
        'value' => EntityAutocomplete::getEntityLabels([$node]),
        'label' => implode(' ', $label),
      ];
    }
    return new JsonResponse($results);
  }

}
