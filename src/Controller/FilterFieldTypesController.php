<?php

namespace Drupal\entitytype_filter\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class of FilterFieldTypesController.
 */
class FilterFieldTypesController extends ControllerBase {

  /**
   * Implements content().
   */
  public function content() {
    $form = \Drupal::formBuilder()->getForm('Drupal\entitytype_filter\Form\FilterFieldTypesForm');
    return $form;
  }

}
